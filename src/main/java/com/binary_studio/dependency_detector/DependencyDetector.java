package com.binary_studio.dependency_detector;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class DependencyDetector {

	private static final HashMap<String, Library> dependenciesMap = new HashMap<>();

	private DependencyDetector() {
	}

	private static void fillDependenciesMap(List<String[]> dependencyList) {
		Library lib;
		for (String[] dependency : dependencyList) {
			lib = dependenciesMap.get(dependency[0]);
			lib.addDependency(dependency[1]);
		}
	}

	private static void createDependenciesMap(List<String> librariesList) {
		for (String libraryName : librariesList) {
			if (!dependenciesMap.containsKey(libraryName)) {
				dependenciesMap.put(libraryName, new Library());
			}
		}
	}

	private static void clearAllMarks() {
		for (Map.Entry<String, Library> currentLibrary : dependenciesMap.entrySet()) {
			currentLibrary.getValue().setMark(false);
		}
	}

	public static boolean canBuild(DependencyList libraries) {
		if (libraries.libraries.isEmpty() || libraries.dependencies.isEmpty()) {
			return true;
		}
		dependenciesMap.clear();
		createDependenciesMap(libraries.libraries);
		fillDependenciesMap(libraries.dependencies);

		for (Map.Entry<String, Library> currentLibrary : dependenciesMap.entrySet()) {
			if (!checkCycleDependency(currentLibrary.getValue())) {
				return false;
			}
			clearAllMarks();
		}
		return true;
	}

	private static boolean checkCycleDependency(Library libraryNode) {
		if (libraryNode.isMark()) {
			return false;
		}
		libraryNode.setMark(true);

		for (String libraryName : libraryNode.getDependencies()) {
			if (!checkCycleDependency(dependenciesMap.get(libraryName))) {
				return false;
			}
		}

		return true;
	}

}
