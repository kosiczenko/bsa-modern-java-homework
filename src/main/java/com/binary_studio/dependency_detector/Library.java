package com.binary_studio.dependency_detector;

import java.util.ArrayList;
import java.util.List;

public class Library {

	private boolean mark;

	private String name;

	private List<String> dependencies;

	public Library() {
		this.dependencies = new ArrayList<>();
	}

	public boolean isMark() {
		return this.mark;
	}

	public void setMark(boolean mark) {
		this.mark = mark;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getDependencies() {
		return this.dependencies;
	}

	public void addDependency(String dependency) {
		this.dependencies.add(dependency);
	}

}
