package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;

	private PositiveInteger powerGridConsumption;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger impactReductionPercent;

	private PositiveInteger shieldRegeneration;

	private PositiveInteger hullRegeneration;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powerGridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {

		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		DefenciveSubsystemImpl defenciveSubsystem = new DefenciveSubsystemImpl();
		defenciveSubsystem.name = name;
		defenciveSubsystem.powerGridConsumption = powerGridConsumption;
		defenciveSubsystem.capacitorConsumption = capacitorConsumption;
		defenciveSubsystem.impactReductionPercent = impactReductionPercent;
		defenciveSubsystem.shieldRegeneration = shieldRegeneration;
		defenciveSubsystem.hullRegeneration = hullRegeneration;

		return defenciveSubsystem;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		AttackAction reducedDamage;
		PositiveInteger reducedDamageValue;
		int impactReductionPercentInt = this.impactReductionPercent.value();
		if (impactReductionPercentInt > 95) {
			impactReductionPercentInt = 95;
		}

		int damageReduction = (int) Math.floor(incomingDamage.damage.value() * impactReductionPercentInt / 100.0);

		reducedDamageValue = PositiveInteger.of(incomingDamage.damage.value() - damageReduction);

		reducedDamage = new AttackAction(reducedDamageValue, incomingDamage.attacker, incomingDamage.target,
				incomingDamage.weapon);

		return reducedDamage;
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegeneration, this.hullRegeneration);
	}

	@Override
	public PositiveInteger getShieldRegeneration() {
		return this.shieldRegeneration;
	}

	@Override
	public PositiveInteger getHullRegeneration() {
		return this.hullRegeneration;
	}

}
