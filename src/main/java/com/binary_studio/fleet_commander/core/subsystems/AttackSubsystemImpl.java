package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger powerGridRequirements;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger optimalSpeed;

	private PositiveInteger optimalSize;

	private PositiveInteger baseDamage;

	public AttackSubsystemImpl(String name, PositiveInteger powergridRequirments, PositiveInteger capacitorConsumption,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {
		this.name = name;
		this.powerGridRequirements = powergridRequirments;
		this.capacitorConsumption = capacitorConsumption;
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powerGridRequirements,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {

		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		return new AttackSubsystemImpl(name, powerGridRequirements, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridRequirements;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {

		double sizeReductionModifier;
		double speedReductionModifier;

		int targetSizeInt = (target.getSize()).value();
		int targetSpeedInt = (target.getCurrentSpeed()).value();

		int optimalSpeedInt = this.optimalSpeed.value();
		int optimalSizeInt = this.optimalSize.value();

		if (targetSizeInt >= optimalSizeInt) {
			sizeReductionModifier = 1;
		}
		else {
			sizeReductionModifier = (double) targetSizeInt / optimalSizeInt;
		}

		if (targetSpeedInt <= optimalSpeedInt) {
			speedReductionModifier = 1;
		}
		else {
			speedReductionModifier = optimalSpeedInt / (2.0 * targetSpeedInt);
		}

		int damage = (int) Math.ceil(this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier));

		return PositiveInteger.of(damage);
	}

	@Override
	public String getName() {
		return this.name;
	}

}
