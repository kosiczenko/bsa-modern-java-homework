package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powerGridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private DefenciveSubsystem defenciveSubsystem;

	private AttackSubsystem attackSubsystem;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powerGridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		DockedShip dockedShip = new DockedShip();
		dockedShip.name = name;
		dockedShip.shieldHP = shieldHP;
		dockedShip.hullHP = hullHP;
		dockedShip.powerGridOutput = powerGridOutput;
		dockedShip.capacitorAmount = capacitorAmount;
		dockedShip.capacitorRechargeRate = capacitorRechargeRate;
		dockedShip.speed = speed;
		dockedShip.size = size;
		return dockedShip;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.attackSubsystem = null;
		}
		else {
			int calculatedPowerConsumption = subsystem.getPowerGridConsumption().value()
					+ ((this.defenciveSubsystem != null) ? this.defenciveSubsystem.getPowerGridConsumption().value() : 0);

			if (calculatedPowerConsumption <= this.powerGridOutput.value()) {
				throw new InsufficientPowergridException(calculatedPowerConsumption - this.powerGridOutput.value());
			}
			this.attackSubsystem = subsystem;
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.defenciveSubsystem = null;
		}
		else {
			int calculatedPowerConsumption = subsystem.getPowerGridConsumption().value()
					+ ((this.attackSubsystem != null) ? (this.attackSubsystem.getPowerGridConsumption()).value() : 0);

			if (calculatedPowerConsumption <= this.powerGridOutput.value()) {
				throw new InsufficientPowergridException(calculatedPowerConsumption - this.powerGridOutput.value());
			}
			this.defenciveSubsystem = subsystem;
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}

		return new CombatReadyShip(this.name,
				this.shieldHP,
				this.hullHP,
				this.powerGridOutput,
				this.capacitorAmount,
				this.capacitorRechargeRate,
				this.speed,
				this.size,
				this.attackSubsystem,
				this.defenciveSubsystem);
	}

}
