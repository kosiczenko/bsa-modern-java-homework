package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private final String name;

	private final PositiveInteger shieldHP;

	private final PositiveInteger hullHP;

	private final PositiveInteger powerGridOutput;

	private final PositiveInteger capacitorAmount;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private int currentShieldHP;

	private int currentHullHP;

	private int currentCapacitorPower;

	private final DefenciveSubsystem defenciveSubsystem;

	private final AttackSubsystem attackSubsystem;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powerGridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powerGridOutput = powerGridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.defenciveSubsystem = defenciveSubsystem;
		this.attackSubsystem = attackSubsystem;
	}

	@Override
	public void endTurn() {
		this.currentCapacitorPower = Math.min(this.currentCapacitorPower + this.capacitorRechargeRate.value(),
				this.capacitorAmount.value());
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		Integer attackConsumption = this.attackSubsystem.getCapacitorConsumption().value();

		if (this.currentCapacitorPower < attackConsumption) {
			return Optional.empty();
		}

		this.currentCapacitorPower -= attackConsumption;

		return Optional.of(new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction dealtDamage = this.defenciveSubsystem.reduceDamage(attack);
		int preDamagedShieldHP = this.currentShieldHP;
		this.currentShieldHP -= dealtDamage.damage.value();
		if (this.currentShieldHP < 0) {
			this.currentShieldHP = 0;
			int damageToHull = dealtDamage.damage.value() - preDamagedShieldHP;
			this.currentShieldHP -= damageToHull;
		}

		if (this.currentShieldHP > 0) {
			return new AttackResult.DamageRecived(dealtDamage.weapon, dealtDamage.damage, dealtDamage.target);
		}
		else {
			return new AttackResult.Destroyed();
		}
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		this.currentCapacitorPower -= this.defenciveSubsystem.getCapacitorConsumption().value();

		if (this.currentCapacitorPower < 0) {
			this.currentCapacitorPower += this.defenciveSubsystem.getCapacitorConsumption().value();
			return Optional.empty();
		}

		int hullHpAfterRegen = this.currentHullHP + this.defenciveSubsystem.getHullRegeneration().value();
		int shieldHpAfterRegen = this.currentHullHP + this.defenciveSubsystem.getShieldRegeneration().value();

		if (hullHpAfterRegen > this.hullHP.value()) {
			hullHpAfterRegen = this.hullHP.value();
		}

		if (shieldHpAfterRegen > this.shieldHP.value()) {
			shieldHpAfterRegen = this.shieldHP.value();
		}

		int actualShieldHpRegenerated = shieldHpAfterRegen - this.currentHullHP;
		int actualHullHpRegenerated = hullHpAfterRegen - this.currentHullHP;
		this.currentHullHP += actualShieldHpRegenerated;
		this.currentHullHP += actualHullHpRegenerated;
		return Optional.of(new RegenerateAction(PositiveInteger.of(actualShieldHpRegenerated),
				PositiveInteger.of(actualHullHpRegenerated)));
	}

}
