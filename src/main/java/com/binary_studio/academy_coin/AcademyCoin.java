package com.binary_studio.academy_coin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class AcademyCoin {

	private static boolean canSell = false;

	private static boolean shouldBuy = true;

	private static int profit = 0;

	private static Integer lowPrice;

	private static Integer highPrice;

	private static Integer day;

	private static ArrayList<Integer> pricesList;

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		pricesList = prices.collect(Collectors.toCollection(ArrayList::new));
		lowPrice = Integer.MAX_VALUE;
		highPrice = 0;
		profit = 0;
		day = 0;
		canSell = false;
		shouldBuy = true;

		while (day < pricesList.size()) {
			if (shouldBuy) {
				buy();
			}
			if (canSell) {
				sell();
			}
		}
		return Math.max(profit, 0);
	}

	private static void buy() {
		Integer price;
		for (int dayIdx = day; dayIdx < pricesList.size(); dayIdx++) {
			price = pricesList.get(dayIdx);
			if (price < lowPrice) {
				lowPrice = price;
				shouldBuy = false;
				canSell = true;
				day = dayIdx + 1;
			}
		}
	}

	private static void sell() {
		Integer price;
		for (int dayIdx = day; dayIdx < pricesList.size(); dayIdx++) {
			price = pricesList.get(dayIdx);
			if (price > highPrice && price > lowPrice) {
				highPrice = price;
				shouldBuy = true;
				canSell = false;
			}
			else if (dayIdx >= day || highPrice > 0) {
				break;
			}
			day = dayIdx + 1;
		}

		if (highPrice != 0 && lowPrice != 0) {
			profit = profit + highPrice - lowPrice;
			highPrice = 0;
			lowPrice = Integer.MAX_VALUE;
		}
	}

	public static void main(String[] args) {
		System.out.println(maxProfit(Arrays.stream(new int[] { 7, 1, 5, 3, 6, 4 }).boxed()));
		System.out.println(maxProfit(Arrays.stream(new int[] { 1, 2, 3, 4, 5 }).boxed()));
		System.out.println(maxProfit(Arrays.stream(new int[] { 7, 6, 4, 3, 1 }).boxed()));
	}

}
